print(__doc__)

import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets
from sklearn.svm import SVC

def irisToNum(date):
    return {
        'Iris-virginica': 1,
        'Iris-setosa': 2,
        'Iris-versicolor': 3,
    }[date]

def read_data(filepath, sep=','):
    dataset = []
    labels = []
    with open(filepath, "r") as f:
        lines = f.readlines()
        for line in lines:
            tmp = []
            if line.startswith('\n') == False:  # filter lines which starts with the symbol '\n'
                sepal_length, sepal_width, petal_length, petal_width, type = line.strip().split(sep)[:]
                dataset.append([float(sepal_length), float(sepal_width), float(petal_length), float(petal_width)])
                labels.append(irisToNum(type))
    return dataset, labels

data, target = read_data("data/irisLearn.data")
test_dataset,test_labels = read_data("data/irisTest.data")

clf = SVC()
clf.fit(data,target)
SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
    decision_function_shape=None, degree=3, gamma='auto', kernel='rbf',
    max_iter=-1, probability=False, random_state=None, shrinking=True,
    tol=0.001, verbose=False)

print(clf.score(test_dataset,test_labels))

goodPredictions=0;
for idx, test in enumerate(test_dataset):
    label = clf.predict(test)
    expected_label = test_labels[idx]
    good = False
    if label == expected_label:
        goodPredictions+=1
        good = True
    print("predicted {0}; expected {1}; bad: {2}".format(label,expected_label,good))


print("good predictions: {0} {1}".format(goodPredictions, goodPredictions*100/len(test_labels)))
bad_predictions = len(test_labels)-goodPredictions
print("bad predictions: {0} {1}".format(bad_predictions, bad_predictions*100/len(test_labels)))