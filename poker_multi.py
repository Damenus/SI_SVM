print(__doc__)

import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets
from sklearn.svm import SVC

hand = ["Nothing in hand","One pair","Two pairs","Three of a kind","Straight","Flush","Full house","Four of a kind","Straight flush","Royal flush"]


# Read data from an file
def read_data(filepath,sep=','):
    dataset = []
    labels = []
    with open(filepath, "r") as f:
        lines = f.readlines()

        for line in lines:
            if line.startswith('\n') == False:  # filter lines which starts with the symbol '\n'
                S1,C1,S2,C2,S3,C3,S4,C4,S5,C5,hand = line.strip().split(sep)[:]
                dataset.append([float(S1),
                                float(C1),
                                float(S2),
                                float(C2),
                                float(S3),
                                float(C3),
                                float(S4),
                                float(C4),
                                float(S5),
                                float(C5)])
                labels.append(int(hand))
    return dataset, labels

data, target = read_data("data/poker-hand-training-true.data")
test_dataset,test_labels = read_data("data/poker-hand-testing.data")

clf = SVC()
clf.fit(data,target)
SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
    decision_function_shape=None, degree=3, gamma='auto', kernel='rbf',
    max_iter=-1, probability=False, random_state=None, shrinking=True,
    tol=0.001, verbose=False)

C = 1.0
svc = svm.SVC(kernel='linear', C=C).fit(data,target)
rbf_svc = svm.SVC(kernel='rbf', gamma=0.7, C=C).fit(data,target)
poly_svc = svm.SVC(kernel='poly', C=C).fit(data,target)
lin_svc = svm.LinearSVC(C=C).fit(data,target)

#Write result
print(clf.score(test_dataset,test_labels))
print(svc.score(test_dataset,test_labels))
print(rbf_svc.score(test_dataset,test_labels))
print(poly_svc.score(test_dataset,test_labels))
print(lin_svc.score(test_dataset,test_labels))
