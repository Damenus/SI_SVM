print(__doc__)

import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets
from sklearn.svm import SVC

def monthToNum(date):
    return{
            'jan' : 1,
            'feb' : 2,
            'mar' : 3,
            'apr' : 4,
            'may' : 5,
            'jun' : 6,
            'jul' : 7,
            'aug' : 8,
            'sep' : 9,
            'oct' : 10,
            'nov' : 11,
            'dec' : 12
    }[date]

def dayToNum(date):
    return{
            'mon' : 1,
            'tue' : 2,
            'wed' : 3,
            'thu' : 4,
            'fri' : 5,
            'sat' : 6,
            'sun' : 7
    }[date]


# Read data from an file
def read_data(filepath,sep=','):
    dataset = []
    labels = []
    with open(filepath, "r") as f:
        lines = f.readlines()
        for line in lines:
            if line.startswith('\n') == False:  # filter lines which starts with the symbol '\n'
                X,Y,month,day,FFMC,DMC,DC,ISI,temp,RH,wind,rain,area  = line.strip().split(sep)[:]
                dataset.append([float(X),
                                float(Y),
                                float(monthToNum(month)),
                                float(dayToNum(day)),
                                float(FFMC),
                                float(DMC),
                                float(DC),
                                float(ISI),
                                float(temp),
                                float(RH),
                                float(wind),
                                float(rain)])
                labels.append(area)
    return dataset, labels



data, target = read_data("data/forestfiresLearn.data")
test_dataset,test_labels = read_data("data/forestfiresTest.data")

clf = SVC()
clf.fit(data,target)
SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
    decision_function_shape=None, degree=3, gamma='auto', kernel='rbf',
    max_iter=-1, probability=False, random_state=None, shrinking=True,
    tol=0.001, verbose=False)

C = 1.0
svc = svm.SVC(kernel='linear', C=C).fit(data,target)
rbf_svc = svm.SVC(kernel='rbf', gamma=0.7, C=C).fit(data,target)
poly_svc = svm.SVC(kernel='poly', C=C).fit(data,target)
lin_svc = svm.LinearSVC(C=C).fit(data,target)

#Write result
print(clf.score(test_dataset,test_labels))
print(svc.score(test_dataset,test_labels))
print(rbf_svc.score(test_dataset,test_labels))
print(poly_svc.score(test_dataset,test_labels))
print(lin_svc.score(test_dataset,test_labels))

