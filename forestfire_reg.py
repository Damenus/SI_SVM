print(__doc__)

import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets
from sklearn.svm import SVC

def monthToNum(date):
    return{
            'jan' : 1,
            'feb' : 2,
            'mar' : 3,
            'apr' : 4,
            'may' : 5,
            'jun' : 6,
            'jul' : 7,
            'aug' : 8,
            'sep' : 9,
            'oct' : 10,
            'nov' : 11,
            'dec' : 12
    }[date]

def dayToNum(date):
    return{
            'mon' : 1,
            'tue' : 2,
            'wed' : 3,
            'thu' : 4,
            'fri' : 5,
            'sat' : 6,
            'sun' : 7
    }[date]


# Read data from an file
def read_data(filepath,sep=','):
    dataset = []
    labels = []
    with open(filepath, "r") as f:
        lines = f.readlines()
        for line in lines:
            if line.startswith('\n') == False:  # filter lines which starts with the symbol '\n'
                X,Y,month,day,FFMC,DMC,DC,ISI,temp,RH,wind,rain,area  = line.strip().split(sep)[:]
                dataset.append([float(X),
                                float(Y),
                                float(monthToNum(month)),
                                float(dayToNum(day)),
                                float(FFMC),
                                float(DMC),
                                float(DC),
                                float(ISI),
                                float(temp),
                                float(RH),
                                float(wind),
                                float(rain)])
                labels.append(float(area))
    return dataset, labels



data, target = read_data("data/forestfiresLearn.data")
test_dataset,test_labels = read_data("data/forestfiresTest.data")

#target = np.asarray(target, dtype="|S6")
#data = np.asarray(data, dtype="|S6")
#test_dataset = np.asarray(test_dataset, dtype="|S6")
#test_labels = np.asarray(test_labels, dtype="|S6")




C = 1.0
svc = svm.SVR(kernel='linear').fit(data,target)
print(svc.score(test_dataset,test_labels))
rbf_svc = svm.SVR(kernel='rbf').fit(data,target)
print(rbf_svc.score(test_dataset,test_labels))

lin_svc = svm.LinearSVR().fit(data,target)
print(lin_svc.score(test_dataset,test_labels))


#poly_svc = svm.SVR(kernel='poly', C=C).fit(data,target)
#print(poly_svc.score(test_dataset,test_labels))
#Write result






