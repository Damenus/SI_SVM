print(__doc__)

import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets
from sklearn.svm import SVC

def irisToNum(date):
    return {
        'Iris-virginica': 1,
        'Iris-setosa': 2,
        'Iris-versicolor': 3,
    }[date]

def read_data(filepath, sep=','):
    dataset = []
    labels = []
    with open(filepath, "r") as f:
        lines = f.readlines()
        for line in lines:
            tmp = []
            if line.startswith('\n') == False:  # filter lines which starts with the symbol '\n'
                sepal_length, sepal_width, petal_length, petal_width, type = line.strip().split(sep)[:]
                dataset.append([float(sepal_length), float(sepal_width), float(petal_length), float(petal_width)])
                labels.append(irisToNum(type))
    return dataset, labels

data, target = read_data("data/irisLearn.data")
test_dataset,test_labels = read_data("data/irisTest.data")

clf = SVC()
clf.fit(data,target)
SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
    decision_function_shape=None, degree=3, gamma='auto', kernel='rbf',
    max_iter=-1, probability=False, random_state=None, shrinking=True,
    tol=0.001, verbose=False)


C = 1.0
svc = svm.SVC(kernel='linear', C=C).fit(data,target)
rbf_svc = svm.SVC(kernel='rbf', gamma=0.7, C=C).fit(data,target)
poly_svc = svm.SVC(kernel='poly', C=C).fit(data,target)
lin_svc = svm.LinearSVC(C=C).fit(data,target)

#Write result
print(clf.score(test_dataset,test_labels))
print(svc.score(test_dataset,test_labels))
print(rbf_svc.score(test_dataset,test_labels))
print(poly_svc.score(test_dataset,test_labels))
print(lin_svc.score(test_dataset,test_labels))

x =0
while x < 10:
    rbf_svc = svm.SVC(kernel='rbf', gamma=x/10, C=C).fit(data,target)
    print(rbf_svc.score(test_dataset,test_labels))
    x += 1

x =0
while x < 10:
    rbf_svc = svm.SVC(kernel='rbf', gamma=x, C=C).fit(data,target)
    print(rbf_svc.score(test_dataset,test_labels))
    x += 1
